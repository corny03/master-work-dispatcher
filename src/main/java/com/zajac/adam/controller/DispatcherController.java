package com.zajac.adam.controller;

import com.zajac.adam.exception.WrongDataException;
import com.zajac.adam.service.DistributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Created by zajac on 19.08.2017.
 * Main controller to Dispatcher feign
 */
@Controller
public class DispatcherController {

    private final DistributeService distributeService;

    @Autowired
    public DispatcherController(DistributeService distributeService) {
        this.distributeService = distributeService;
    }

    /**
     * @param imageUrl hyperlink to image
     * @param userName name of user who send file
     * @param isHidden boolean parameter describes that image should be visible on main site
     * @return ResponseEntity with codes:
     * OK 200 - when photo was deleted in this session
     * Internal Server Error 500 - when uploading to main data service failed
     */
    @PostMapping("/uploadFromLink")
    public ResponseEntity<String> singleUpload(@RequestParam("imageUrl") String imageUrl,
                                               @RequestParam("userName") String userName,
                                               @RequestParam("isHidden") boolean isHidden) {
        String fileName = getFilenameFromUrl(imageUrl);
        checkFileName(fileName);
        byte[] bytes = downloadImage(imageUrl);
        checkFileSize(bytes);
        String idImage = distributeService.distributeToMicroServices(fileName, bytes, userName, isHidden);

        return ResponseEntity.ok(idImage);
    }

    /**
     * @param fileName name of image
     * @param userName name of user who send file
     * @param isHidden boolean parameter describes that image should be visible on main site
     * @param bytes    image data
     * @return ResponseEntity with codes:
     * OK 200 - when photo was deleted in this session
     * Internal Server Error 500 - when uploading to main data service failed
     * Method used to sending through feign by other microservices, actual in internal functional tests
     */
    @PostMapping("/upload")
    public ResponseEntity<String> singleUpload(@RequestParam("filename") String fileName,
                                               @RequestBody byte[] bytes,
                                               @RequestParam("userName") String userName,
                                               @RequestParam(value = "isHidden", required = false, defaultValue = "false") Boolean isHidden) {
        String idImage = distributeService.distributeToMicroServices(fileName, bytes, userName, isHidden);
        return ResponseEntity.ok(idImage);
    }

    /**
     * @param file     data with image
     * @param userName name of user who send file
     * @param isHidden boolean parameter describes that image should be visible on main site
     * @return ResponseEntity with codes:
     * OK 200 - when photo was deleted in this session
     * Bad Request 400 - When is bad format of photo, or body is empty
     * Internal Server Error 500 - when uploading to main data service failed
     */
    @PostMapping("/uploadMultipartFile")
    public ResponseEntity<String> singleUpload(@RequestPart("file") MultipartFile file,
                                               @RequestParam("userName") String userName,
                                               @RequestParam("isHidden") Boolean isHidden) {
        byte[] bytes = getBytesFromMultipart(file);
        String fileName = getFileNameFromMultipartFile(file);
        String idImage = distributeService.distributeToMicroServices(fileName, bytes, userName, isHidden);
        return ResponseEntity.ok(idImage);
    }

    /**
     * @param id identifier of image
     * @return ResponseEntity with codes:
     * OK 200 - when photo was deleted in this session
     * Gone 410 - when photo is not in service
     * Internal Server Error 500 - when other problems
     */
    @DeleteMapping("/delete-image/{id}")
    public ResponseEntity deleteById(@PathVariable String id) {
        distributeService.deleteImageFromMicroServicesById(id);
        return ResponseEntity.ok("deleted");
    }

    private byte[] downloadImage(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            BufferedImage image = ImageIO.read(url);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", baos);
            baos.flush();
            byte[] bytesToReturn = baos.toByteArray();
            baos.close();
            return bytesToReturn;
        } catch (IOException e) {
            throw new WrongDataException("Cant download file from link");
        }
    }

    private byte[] getBytesFromMultipart(MultipartFile file) {
        byte[] bytes;
        if (file.isEmpty())
            throw new WrongDataException("Data of sent file is empty");
        try {
            bytes = file.getBytes();
            if (!isBytesOk(bytes))
                throw new WrongDataException("Data of sent file is wrong");
        } catch (IOException e) {
            e.printStackTrace();
            throw new WrongDataException("Other data problem");
        }
        return bytes;
    }

    private String getFileNameFromMultipartFile(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        checkFileName(fileName);
        return fileName;
    }

    private boolean isBytesOk(byte[] bytes) {
        return bytes != null && bytes.length > 1;
    }

    private void checkFileName(String fileName) {
        if (!(fileName.toLowerCase().contains(".jpg")
                || fileName.toLowerCase().contains(".jpeg")
                || fileName.toLowerCase().contains(".png")))
            throw new WrongDataException("Bad format of data");
    }

    private void checkFileSize(byte[] bytes) {
        final int oneMegaByteInBytes = 1048576;
        if (bytes.length > oneMegaByteInBytes)
            throw new WrongDataException("too big file");
        if (!isBytesOk(bytes)) {
            throw new WrongDataException("empty data");
        }
    }

    private String getFilenameFromUrl(String imageUrl) {
        String[] splitedUrl = imageUrl.split("/");
        return splitedUrl[splitedUrl.length - 1];
    }
}
