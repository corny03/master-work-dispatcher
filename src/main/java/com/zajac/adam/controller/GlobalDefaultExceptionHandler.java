package com.zajac.adam.controller;

import com.zajac.adam.exception.OtherMicroServiceNotWorkException;
import com.zajac.adam.exception.WrongDataException;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by zajac on 29.11.2017.
 * global place for handling exceptions
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {

    @ExceptionHandler(WrongDataException.class)
    public ResponseEntity handleException(Exception e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler(OtherMicroServiceNotWorkException.class)
    public ResponseEntity handleOtherMicroServiceNotWorkException(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }

    @ExceptionHandler(value = FeignException.class)
    public ResponseEntity defaultFeignHandler(FeignException e) {
        if (e.status() == 410)
            return ResponseEntity.status(e.status()).body("image was deleted before");
        else
            return ResponseEntity.status(e.status()).body(e.getMessage());
    }
}
