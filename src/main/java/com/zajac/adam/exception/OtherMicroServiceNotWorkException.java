package com.zajac.adam.exception;

/**
 * Created by zajac on 29.11.2017.
 * thrown when other micro service have problem
 */
public class OtherMicroServiceNotWorkException extends RuntimeException {
    public OtherMicroServiceNotWorkException() {
        super();
    }

    public OtherMicroServiceNotWorkException(String message, Throwable cause) {
        super(message, cause);
    }

    public OtherMicroServiceNotWorkException(String message) {
        super(message);
    }

    public OtherMicroServiceNotWorkException(Throwable cause) {
        super(cause);
    }
}
