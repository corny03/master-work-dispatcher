package com.zajac.adam.exception;

/**
 * Created by zajac on 29.11.2017.
 * when delivered data is wrong
 */
public class WrongDataException extends RuntimeException {
    public WrongDataException() {
        super();
    }

    public WrongDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongDataException(String message) {
        super(message);
    }

    public WrongDataException(Throwable cause) {
        super(cause);
    }
}
