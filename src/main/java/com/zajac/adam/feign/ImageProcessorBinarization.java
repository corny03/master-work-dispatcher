package com.zajac.adam.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zajac on 19.08.2017.
 * Feign client to filter
 */
@FeignClient(name = "image-processor-binarization")
public interface ImageProcessorBinarization {

    @PostMapping("/upload")
    ResponseEntity singleUpload(@RequestParam("id") String id, @RequestParam("filename") String fileName, @RequestBody byte[] bytes);

    @DeleteMapping("/{id}")
    ResponseEntity deleteById(@PathVariable("id") String id);

    @DeleteMapping("/all/{id}")
    ResponseEntity deleteAllByOriginalId(@PathVariable("id") String id);
}