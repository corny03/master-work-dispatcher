package com.zajac.adam.service;

import com.zajac.adam.feign.ImageProcessorBinarization;
import com.zajac.adam.feign.ImageProcessorColorsBinarization;

/**
 * Created by zajac on 07.09.2017.
 * Is used to send image to other image microservices
 */
public class BroadcastThread implements Runnable {
    private final String idSourceImage;
    private final String fileName;
    private final byte[] bytes;
    private final ImageProcessorBinarization imageProcessorBinarization;
    private final ImageProcessorColorsBinarization imageProcessorColorsBinarization;

    BroadcastThread(String idSourceImage, String fileName, byte[] bytes,
                    ImageProcessorBinarization imageProcessorBinarization,
                    ImageProcessorColorsBinarization imageProcessorColorsBinarization) {
        this.idSourceImage = idSourceImage;
        this.fileName = fileName;
        this.bytes = bytes;
        this.imageProcessorBinarization = imageProcessorBinarization;
        this.imageProcessorColorsBinarization = imageProcessorColorsBinarization;
    }

    @Override
    public void run() {
        //Can add if statesment, when is not saved in some processor then save in some databese log
        imageProcessorBinarization.singleUpload(idSourceImage, fileName, bytes); //can be logged
        imageProcessorColorsBinarization.singleUpload(idSourceImage, fileName, bytes);
    }
}
