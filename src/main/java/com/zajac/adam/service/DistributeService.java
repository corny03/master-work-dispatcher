package com.zajac.adam.service;

import com.zajac.adam.exception.OtherMicroServiceNotWorkException;
import com.zajac.adam.feign.DataSource;
import com.zajac.adam.feign.ImageProcessorBinarization;
import com.zajac.adam.feign.ImageProcessorColorsBinarization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by zajac on 22.11.2017.
 * Model of controller
 */

@Service
public class DistributeService {

    private final ImageProcessorBinarization imageProcessorBinarization;
    private final ImageProcessorColorsBinarization imageProcessorColorsBinarization;
    private final DataSource dataSource;

    @Autowired
    public DistributeService(ImageProcessorBinarization imageProcessorBinarization,
                             ImageProcessorColorsBinarization imageProcessorColorsBinarization,
                             DataSource dataSource) {
        this.imageProcessorBinarization = imageProcessorBinarization;
        this.imageProcessorColorsBinarization = imageProcessorColorsBinarization;
        this.dataSource = dataSource;
    }

    public void deleteImageFromMicroServicesById(String id) {
        dataSource.deleteById(id);
        imageProcessorBinarization.deleteAllByOriginalId(id);
    }

    public String distributeToMicroServices(String fileName, byte[] bytes, String userName, boolean isHidden) {
        ResponseEntity<String> idCreatedImageResponseEntity = dataSource.singleUpload(fileName, bytes, userName, isHidden);
        if (idCreatedImageResponseEntity.getStatusCode() != HttpStatus.CREATED)
            throw new OtherMicroServiceNotWorkException("problem with uploading to image source");
        String idOriginalImage = idCreatedImageResponseEntity.getBody();
//        distributeToRestServicesInBackground(idOriginalImage, fileName, bytes);

        return idOriginalImage;
    }

    private void distributeToRestServicesInBackground(String idOriginalImage, String fileName, byte[] bytes) {
        Thread uploadThread = new Thread(new BroadcastThread(idOriginalImage, fileName, bytes,
                imageProcessorBinarization, imageProcessorColorsBinarization));
        uploadThread.start();
    }
}
