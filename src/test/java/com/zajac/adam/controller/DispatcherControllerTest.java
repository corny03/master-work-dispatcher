package com.zajac.adam.controller;

import com.zajac.adam.service.DistributeService;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.LinkedHashMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by zajac on 06.12.2017.
 * Controller Tests
 */
public class DispatcherControllerTest {
    private DispatcherController dispatcherController;
    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();
    @Mock
    private DistributeService distributeService;
    @Mock
    private MockMvc mockMvc;
    @Before
    public void setUp() throws Exception {
        this.distributeService = Mockito.mock(DistributeService.class);
        this.dispatcherController = new DispatcherController(this.distributeService);
        mockMvc = MockMvcBuilders.standaloneSetup(this.dispatcherController)
                .setControllerAdvice(new GlobalDefaultExceptionHandler())
                .build();
    }

    @Test
    public void singleUpload() throws Exception {
        //given
        byte[] sampleBytes = {23, 12, 33, 4, 111, 122, 11, 9, 4, 4};
        String fileName = "goodFileName.jpg";
        Boolean isHidden = false;
        final MockMultipartFile mockMultipartFile =
                new MockMultipartFile("file", fileName, "multipart/form-data", sampleBytes);
        String returnId = "3L";
        String userName = "testUser";
        Mockito.when(distributeService.distributeToMicroServices(fileName, sampleBytes, userName, false))
                .thenReturn(returnId);
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("userName", userName);
        params.add("isHidden", isHidden.toString());
        //when
        ResultActions okResultActions = mockMvc.perform(
                fileUpload("/uploadMultipartFile").file(mockMultipartFile).params(params));
        //then
        okResultActions.andExpect(status().isOk()).andExpect(MockMvcResultMatchers.content().string(returnId));
    }

    @Test
    public void singleUploadNullBytes() throws Exception {
        //given
        byte[] sampleBytes = {23, 12, 33, 4, 111, 122, 11, 9, 4, 4};
        byte[] nullBytes = null;
        String fileName = "goodFileName.jpg";
        Boolean isHidden = false;
        final MockMultipartFile mockMultipartFileNullBytes = new MockMultipartFile(fileName, nullBytes);
        String returnId = "3L";
        String userName = "testUser";
        Mockito.when(distributeService.distributeToMicroServices(fileName, sampleBytes, userName, false))
                .thenReturn(returnId);
        //when
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("userName", userName);
        params.add("isHidden", isHidden.toString());
        ResultActions nullBytesResultActions = mockMvc.perform(
                fileUpload("/uploadMultipartFile")
                        .file(mockMultipartFileNullBytes)
                        .params(params));
        //then
        nullBytesResultActions.andExpect(status().isBadRequest());
    }

    @Test
    public void singleUploadBadName() throws Exception {
        //given
        byte[] sampleBytes = {23, 12, 33, 4, 111, 122, 11, 9, 4, 4};
        String fileName = "goodFileName.jpg";
        String badFileName = "badFormatFileName.txt";
        Boolean isHidden = false;
        final MockMultipartFile mockMultipartFileBadName = new MockMultipartFile(badFileName, sampleBytes);
        String returnId = "3L";
        String userName = "testUser";
        Mockito.when(distributeService.distributeToMicroServices(fileName, sampleBytes, userName, false))
                .thenReturn(returnId);
        //when
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("userName", userName);
        params.add("isHidden", isHidden.toString());

        ResultActions badNameResultActions = mockMvc.perform(
                fileUpload("/uploadMultipartFile")
                        .file(mockMultipartFileBadName)
                        .params(params));
        //then
        badNameResultActions.andExpect(status().isBadRequest());
    }

    @Test
    public void singleUploadByUrlGood() throws Exception {
        //given
        String userName = "testUser";
        Boolean isHidden = false;
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("userName", userName);
        params.add("isHidden", isHidden.toString());
        params.add("imageUrl",
                "https://n6-img-fp.akamaized.net/free-icon/jpg-file-format-variant_318-45505.jpg?size=338&ext=jpg");
        //when
        final ResultActions goodResult = mockMvc.perform(post("/uploadFromLink").params(params));
        //then
        goodResult.andExpect(status().isOk());
    }

    @Test
    public void singleUploadByUrlBad() throws Exception {
        //given
        String userName = "testUser";
        Boolean isHidden = false;
        final MultiValueMap<String, String> paramsWrongUrl = new LinkedMultiValueMap<>();
        paramsWrongUrl.add("userName", userName);
        paramsWrongUrl.add("isHidden", isHidden.toString());
        paramsWrongUrl.add("imageUrl", "this is wrong url");
        //when
        final ResultActions badResult = mockMvc.perform(post("/uploadFromLink").params(paramsWrongUrl));
        //then
        badResult.andExpect(status().isBadRequest());
    }

    @Test
    public void deleteByIdExisting() throws Exception {
        //given
        String goodId = "5L";
        //when
        final ResultActions deleteResult = mockMvc.perform(delete("/delete-image/" + goodId));
        //then
        deleteResult.andExpect(status().isOk());
    }

    @Test
    public void deleteByIdDeleted() throws Exception {
        //given
        String deletedOrNotExistedId = "59L";
        Response response = Response.builder()
                .status(410)
                .reason("GONE")
                .headers(new LinkedHashMap<>())
                .build();
        Exception exception = errorDecoder.decode("Service#foo()", response);
        Mockito.doThrow(exception).when(distributeService).deleteImageFromMicroServicesById(deletedOrNotExistedId);
        //when
        final ResultActions noExistdeleteResult = mockMvc.perform(
                delete("/delete-image/" + deletedOrNotExistedId));
        //then
        noExistdeleteResult.andExpect(status().isGone());
    }
}