package com.zajac.adam.service;

import com.zajac.adam.controller.GlobalDefaultExceptionHandler;
import com.zajac.adam.feign.DataSource;
import com.zajac.adam.feign.ImageProcessorBinarization;
import com.zajac.adam.feign.ImageProcessorColorsBinarization;
import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.LinkedHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by zajac on 06.12.2017.
 * Service test
 */
public class DistributeServiceTest {
    private DistributeService distributeService;
    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();
    private GlobalDefaultExceptionHandler globalDefaultExceptionHandler = new GlobalDefaultExceptionHandler();
    @Mock
    private ImageProcessorBinarization imageProcessorBinarization;
    @Mock
    private ImageProcessorColorsBinarization imageProcessorColorsBinarization;
    @Mock
    private DataSource dataSource;

    @Before
    public void setUp() throws Exception {
        imageProcessorBinarization = Mockito.mock(ImageProcessorBinarization.class);
        imageProcessorColorsBinarization = Mockito.mock(ImageProcessorColorsBinarization.class);
        dataSource = Mockito.mock(DataSource.class);
        this.distributeService = new DistributeService(imageProcessorBinarization,
                imageProcessorColorsBinarization, dataSource);
    }

    @Test
    public void deleteImageFromMicroServicesById() throws Exception {
        //given
        String idExistedImage = "1L";
        Response response = Response.builder()
                .status(410)
                .reason("GONE")
                .headers(new LinkedHashMap<>())
                .build();

        ResponseEntity<Object> okResponse = ResponseEntity.ok().build();
        Mockito.when(dataSource.deleteById(idExistedImage)).thenReturn(okResponse);
        //when
        boolean imageDeleted = true;
        try {
            distributeService.deleteImageFromMicroServicesById(idExistedImage);
        } catch (Exception e) {
            imageDeleted = false;
        }
        //then
        assertTrue(imageDeleted);
    }

    @Test
    public void deleteImageFromMicroServicesByIdDeleted() throws Exception {
        //given
        String idNotExistedImage = "2L";
        Response response = Response.builder()
                .status(410)
                .reason("GONE")
                .headers(new LinkedHashMap<>())
                .build();
        Exception exception = errorDecoder.decode("Service#foo()", response);
        Mockito.when(dataSource.deleteById(idNotExistedImage)).thenThrow(exception);
        //when
        ResponseEntity notDeletedResponseEntity = ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).build();
        try {
            distributeService.deleteImageFromMicroServicesById(idNotExistedImage);
        } catch (FeignException e) {
            notDeletedResponseEntity = globalDefaultExceptionHandler.defaultFeignHandler(e);
        }
        //then
        assertEquals(HttpStatus.GONE, notDeletedResponseEntity.getStatusCode());
    }

    @Test
    public void distributeToMicroServices() throws Exception {
        //given
        String goodFileName = "goodFileName.jpg";
        byte[] bytes = {23, 1, 12, 4, 3, 1, 2, 3, 4, 5, 11, 4};
        String expectedString = "1L";
        String userName = "testUser";
        ResponseEntity<String> expectedDataSourceResponse = ResponseEntity.status(HttpStatus.CREATED).body(expectedString);
        Mockito.when(dataSource.singleUpload(goodFileName, bytes, userName, false)).thenReturn(expectedDataSourceResponse);
        //when
        String resultString = distributeService.distributeToMicroServices(goodFileName, bytes, userName, false);
        //then
        assertEquals(expectedString, resultString);
    }
}